package main

import (
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"gopkg.in/yaml.v2"
)

type Config struct {
	Sources []map[string]interface{} `yaml:"sources"`
}

func main() {
	app := fiber.New()

	yamlFilePath := os.Getenv("APP_CONFIG_PATH")
	if yamlFilePath == "" {
		yamlFilePath = "./config.yaml"
	}

	// Read YAML file
	yamlData, err := os.ReadFile(yamlFilePath)
	if err != nil {
		log.Fatalf("Error reading YAML file: %v", err)
	}

	// Unmarshal YAML data into Config struct
	var config Config
	if err := yaml.Unmarshal(yamlData, &config); err != nil {
		log.Fatalf("Error unmarshalling YAML data: %v", err)
	}

	// Define endpoint to serve sources
	app.Get("/sources", func(c *fiber.Ctx) error {
		return c.JSON(config.Sources)
	})

	// Start the server
	if err := app.Listen(":3000"); err != nil {
		log.Fatalf("Error starting server: %v", err)
	}
}
