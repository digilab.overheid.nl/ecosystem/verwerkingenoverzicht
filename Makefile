-include ./local_*/Makefile

# TODO: give short lowercase (preferably no hyphens or underscores) project name.
PROJECT_NAME := verwerkingenoverzicht
CLUSTER_EXISTS := $$(k3d cluster list $(PROJECT_NAME) --no-headers | wc -l | xargs)
ROOT_DIR := ${PWD}

.PHONY: k3d
k3d:
	@if [ $(CLUSTER_EXISTS) -eq 0 ]; then \
		KUBECONFIG= k3d cluster create --config=deploy/k3d/config.yaml; \
	else \
		k3d cluster start "$(PROJECT_NAME)"; \
	fi
	kubectl config use-context "k3d-$(PROJECT_NAME)"

.PHONY: dev
dev: k3d
	skaffold dev --cleanup=false

.PHONY: stop
stop:
	k3d cluster stop "$(PROJECT_NAME)"

.PHONY: clean
clean:
	k3d cluster delete "$(PROJECT_NAME)"

.PHONY: lint
lint:
	@for f in "${ROOT_DIR}"/*/Makefile; do \
		cd "$$(dirname "$$f")" ; \
		if $(MAKE) -q -s lint 2> /dev/null ; test $$? -le 1 ; then \
			$(MAKE) lint ; \
		fi \
	done

.PHONY: tidy
tidy:
	@for f in */go.mod; do \
		( cd "$$(dirname "$$f")"; go mod tidy ) || exit 1; \
	done
