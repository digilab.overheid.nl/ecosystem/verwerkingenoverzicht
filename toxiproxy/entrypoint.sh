#!/bin/sh

# Startup server
toxiproxy-server -config /app/config.json &
JOB_PID=$!

echo "Waiting for toxiproxy to start..." 
# Wait for server to start
while ! nc -z localhost 8474; do
    echo "not ready yet..."   
    sleep 1
done

sleep 5;

# Setup config
echo "add toxic latency"
toxiproxy-cli toxic add \
    --type=latency \
    --downstream \
    --toxicity=0.9 \
    --attribute latency=3000 \
    --attribute jitter=2000 \
    verwerkingenoverzicht

echo "add toxic slow_close"
toxiproxy-cli toxic add \
    --type=slow_close \
    --downstream \
    --toxicity=0.10 \
    --attribute delay=120 \
    verwerkingenoverzicht

echo "add toxic slicer"
toxiproxy-cli toxic add \
    --type=slicer \
    --downstream \
    --toxicity=0.10 \
    --attribute average_size=128 \
    --attribute size_variation=32 \
    --attribute delay=2000 \
    verwerkingenoverzicht

echo "add toxic reset_peer"
toxiproxy-cli toxic add \
    --type=reset_peer \
    --downstream \
    --toxicity=0.05 \
    --attribute timeout=66 \
    verwerkingenoverzicht

echo "add toxic http_status"
toxiproxy-cli toxic add \
    --type=http_status \
    --downstream \
    --toxicity=1.0 \
    --attribute status_code=418 \
    --attribute toxicity=0.25 \
    verwerkingenoverzicht

# Call server to foreground
wait $JOB_PID
