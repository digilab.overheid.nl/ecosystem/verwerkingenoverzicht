// Ported from https://github.com/xthexder/toxic-example/blob/master/http.go
package main

import (
	"bufio"
	"bytes"
	"io"
	"math/rand"
	"net/http"

	"github.com/Shopify/toxiproxy/v2/stream"
	"github.com/Shopify/toxiproxy/v2/toxics"
	"github.com/rs/zerolog/log"
)

type HttpToxic struct {
	StatusCode int     `json:"status_code"` // StatusCode is the HTTP status code to be returned.
	Toxicity   float32 `json:"toxicity"`    // Toxicity is the probability of the toxic being applied to a request (duplicated to function on request level in stead of packet level).
}

func (t *HttpToxic) GetBufferSize() int {
	return 32 * 1024
}

// ModifyResponse modifies the HTTP response to return the status code specified in the toxic.
func (t *HttpToxic) ModifyResponse(resp *http.Response) {

	// Only apply the toxic in a certain percentage of cases
	if rand.Float32() > t.Toxicity {
		return
	}

	log.
		Debug().
		Int("http_status", t.StatusCode).
		Msg("Modifying response")

	resp.StatusCode = t.StatusCode
	resp.Status = http.StatusText(t.StatusCode)

	// Empty the repsponse body
	resp.Body = http.NoBody
	// Update the ContentLength to 0
	resp.ContentLength = 0
	// Remove all headers
	resp.Header = make(http.Header)
	// Add a Content-Length header
	resp.Header.Add("Content-Length", "0")
}

// Pipe reads from the input stream, modifies the HTTP response, and writes to the output stream.
func (t *HttpToxic) Pipe(stub *toxics.ToxicStub) {
	buffer := bytes.NewBuffer(make([]byte, 0, 32*1024))
	writer := stream.NewChanWriter(stub.Output)
	reader := stream.NewChanReader(stub.Input)
	reader.SetInterrupt(stub.Interrupt)
	for {
		tee := io.TeeReader(reader, buffer)
		resp, err := http.ReadResponse(bufio.NewReader(tee), nil)

		switch {
		// If interrupted, write the buffer to the writer and return
		case err == stream.ErrInterrupted:
			buffer.WriteTo(writer)
			return
		// If EOF, close the stub and return
		case err == io.EOF:
			stub.Close()
			return
		// If there's an error, write the buffer to the writer
		case err != nil:
			buffer.WriteTo(writer)
		// If no error, modify the response and write it to the writer
		case err == nil:
			t.ModifyResponse(resp)
			resp.Write(writer)
		}

		// Reset the buffer for the next iteration.
		buffer.Reset()
	}
}

// init registers the HttpToxic struct with the toxics package.
func init() {
	toxics.Register("http_status", new(HttpToxic))
}
