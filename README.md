# Verwerkingenoverzicht

TODO: Project short description

## Overview

TODO: Project overview.

## Running the project

TODO: describe, as concise as possible, how to use the project.

## Developer documentation

If you would like to contribute to this project, consult the [`CONTRIBUTING.md`](CONTRIBUTING.md) file.

## Deployment

TODO: Describe how this project should be deployed.

## License

Copyright © VNG Realisatie 2024

[Licensed under the EUPLv1.2](LICENSE)

[You can find more information about the license here](https://commission.europa.eu/content/european-union-public-licence_en).
