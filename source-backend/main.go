package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
)

type Verwerking struct {
	Log string
}

func main() {
	app := fiber.New()

	// Define endpoint to serve sources
	app.Get("/verwerkingen", func(c *fiber.Ctx) error {
		return c.JSON([]Verwerking{
			{Log: "foo"},
			{Log: "bar"},
		})
	})

	// Start the server
	if err := app.Listen(":3000"); err != nil {
		log.Fatalf("Error starting server: %v", err)
	}
}
